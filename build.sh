#!/bin/bash

set -e

export INSTALL="$(pwd)/install"
export DOWNLOADS="$(pwd)/downloads"
export DEB_VERSION=6.0.0
export FSMAJORVERSION=6

mkdir -p $INSTALL
mkdir -p $DOWNLOADS
rm -rf $INSTALL/*

[ -f $DOWNLOADS/freesurfer.tar.gz ] || wget -v -L -O  $DOWNLOADS/freesurfer.tar.gz "https://surfer.nmr.mgh.harvard.edu/pub/dist/freesurfer/6.0.0/freesurfer-Linux-centos6_x86_64-stable-pub-v6.0.0.tar.gz" 2> wget.log

cd $INSTALL
mkdir -p opt
cd opt
tar xzvf $DOWNLOADS/freesurfer.tar.gz > ../untar.log
if [ -n "$CI" ]; then
  rm -f $DOWNLOADS/freesurfer.tar.gz
fi
sed -i -e 's#freeview.bin#$FREESURFER_HOME/bin/freeview.bin#' freesurfer/bin/freeview
cp ../../fslogosmall.png freesurfer/

cd $INSTALL
mkdir -p etc/profile.d/
cat <<EOF > etc/profile.d/freesurfer.sh
# FREESURFER
export FREESURFER_HOME=/opt/freesurfer
if [ "\$USER" != "root" ]; then
  source \$FREESURFER_HOME/SetUpFreeSurfer.sh
fi
export PATH=\$PATH:\$FREESURFER_HOME/bin
EOF

cd $INSTALL
mkdir -p usr/share/applications
cat <<EOF > usr/share/applications/Freeview.desktop
[Desktop Entry]
Type=Application
Name=Freeview
Icon=/opt/freesurfer/fslogosmall.png
Exec=/opt/freesurfer/bin/freeview
Terminal=false
Categories=Utility;Science;MedicalSoftware;
EOF

cd ..

fpm -s dir -t deb -n freesurfer -v $DEB_VERSION \
   --description="freesurfer - analysis and visualization of structural and functional neuroimaging data from cross-sectional or longitudinal studies" \
   --after-install after-install.sh \
   --before-remove before-remove.sh \
   -d libpulse-dev \
   -d libnss3 \
   -d libglu1-mesa \
   -d libpng12-0 \
  -C $INSTALL opt/ etc/ usr/
